USE test_schema;

#Create user table first, then lookup tables, then item, item_trade, and post_history
#due to foreign key dependencies that need to be created
CREATE TABLE user_info (
`user_id` int(10) NOT NULL AUTO_INCREMENT,
`public_user_id` int(10),
`primary_email` varchar(100) NOT NULL UNIQUE,
`secondary_email` varchar(100),
`password` varchar(100) NOT NULL,
`phone_number` varchar(15),
`first_name` varchar(50),
`last_name` varchar(50),
`date_of_birth` date,
`address` varchar(100),
`city` varchar(50),
`state` char(2),
`postal_code` varchar(12),
`karma_score` int(10),
PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#Creates category table. Acts as a lookup table.
CREATE TABLE item_category (
`category_id` int(10) NOT NULL AUTO_INCREMENT,
`name` varchar(50) NOT NULL,
`description` varchar(100),
PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#Creates condition table. Acts as a lookup table.
CREATE TABLE item_condition (
`condition_id` int(10) NOT NULL AUTO_INCREMENT,
`name` varchar(50) NOT NULL,
`description` varchar(100),
PRIMARY KEY (`condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#Create foreign key restraints to user_id (for seller_id),
#category_id (for two attributes), and condition_id.
CREATE TABLE item (
`item_id` int(10) NOT NULL AUTO_INCREMENT,
`seller_id` int(10),
`category_id` int(10),
`desired_category_id` int(10),
`condition_id` int(10),
`item_name` varchar(100) NOT NULL,
`description` text NOT NULL,
`pictures` varchar(255) NOT NULL,
`estimated_value` decimal,
`isbn-10` char(10),
`isbn-13` char(13),
`item_brand` varchar(50),
`post_date` date,
`isPublic` boolean,
PRIMARY KEY (`item_id`),
FOREIGN KEY (seller_id) REFERENCES user_info(user_id),
FOREIGN KEY (category_id) REFERENCES item_category(category_id),
FOREIGN KEY (desired_category_id) REFERENCES item_category(category_id),
FOREIGN KEY (condition_id) REFERENCES item_condition(condition_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#Create foreign key restraints to user_id, category_id, and condition_id
CREATE TABLE item_trade (
`trade_id` int(10) NOT NULL AUTO_INCREMENT,
`user_id` int(10),
`category_id` int(10),
`condition_id` int(10),
`item_name` varchar(50),
`description` text,
`pictures` varchar(255),
`ISBN-10` char(10),
`ISBN-13` char(13),
`item_brand` varchar(50),
PRIMARY KEY (`trade_id`),
FOREIGN KEY (user_id) REFERENCES user_info(user_id),
FOREIGN KEY (category_id) REFERENCES item_category(category_id),
FOREIGN KEY (condition_id) REFERENCES item_condition(condition_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#Create foreign key restraints to user_id (for seller_id and buyer_id),
#item_id, and trade_id
CREATE TABLE post_history (
`post_id` int(10) NOT NULL AUTO_INCREMENT,
`seller_id` int(10),
`buyer_id` int(10),
`item_id` int(10),
`trade_id` int(10),
`payment` decimal,
`shipping_method` varchar(50),
`buy_date` date,
`ship_date` date,
`isSold` boolean,
PRIMARY KEY (`post_id`),
FOREIGN KEY (seller_id) REFERENCES user_info(user_id),
FOREIGN KEY (buyer_id) REFERENCES user_info(user_id),
FOREIGN KEY (item_id) REFERENCES item(item_id),
FOREIGN KEY (trade_id) REFERENCES item_trade(trade_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#Sample insert statements that were used to test foreign key restraints
INSERT INTO user_info (`primary_email`, `password`) VALUES ('kayla@offure.com', 'test123'),
('andrew@offure.com', 'test321'),
('joshua@offure.com', 'testing'),
('kalyani@offure.com', 'test456'),
('braden@offure.com', '123test'),
('kaixiang@offure.com', '456test'),
('test@offure.com', 'test1233433'),
('test123@offure.com', 'test33');

INSERT INTO item_category (`name`, `description`) VALUES ('Books', 'Test description'),
('Video games', 'Current generation video games'),
('Computers', 'Laptop and desktop computers');

INSERT INTO item_condition (`name`, `description`) VALUES ('Brand New', 'Brand spanking new'),
('Very Good', 'Close to new'),
('Horrible', 'Literally trash');

INSERT INTO item (`seller_id`, `category_id`, `desired_category_id`,
 `condition_id`, `item_name`, `description`, `pictures`)
 VALUES (1, 3, 3, 2, 'Test item', 'Test description', 'Test picture location'),
 (2, 2, 3, 1, 'Vidja game', 'It\'s fun', 'Pics here'),
 (3, 1, 1, 2, 'Science book', 'I learned stuff', 'Pics here');

INSERT INTO item_trade(`user_id`, `category_id`, `condition_id`, `item_name`)
VALUES (1, 2, 3, 'Test trade item'),
(2, 3, 1, 'iPod thing'),
(3, 1, 3, 'Chemistry book');

INSERT INTO post_history(`seller_id`, `buyer_id`, `item_id`, `trade_id`, `payment`)
VALUES (1, NULL, 1, NULL, NULL),
(2, 1, 2, 4, NULL),
(3, 1, 3, NULL, 23.00);

#Select statements for all tables
SELECT * FROM user_info;
SELECT * FROM item_category;
SELECT * FROM item_condition;
SELECT * FROM item;
SELECT * FROM item_trade;
SELECT * FROM post_history;